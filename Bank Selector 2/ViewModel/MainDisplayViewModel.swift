//
//  ContentViewModel.swift
//  Bank Selector 2
//
//  Created by Laurynas Kapacinskas on 2021-08-31.
//

import SwiftUI
import RealmSwift

class MainDisplayViewModel: ObservableObject {
    
    @Published var selectedBankName: String?
    @Published var banks = [BankDB]()
    @ObservedObject var dbViewModel = DBViewModel()
    
    init() {
        filterForSelected()
        downloadData()
    }
    
    func downloadData() {
        NetworkManager.shared.getBankList { (result) in
            switch result{
            case .success(let response):
                self.banks = response
                self.dbViewModel.banks = self.banks
                self.dbViewModel.addData()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func filterForSelected() {
        guard let realm = try? Realm() else {return}
        for bank in realm.objects(BankDB.self).filter("selected == true"){
            self.selectedBankName = bank.name
        }
    }
}

