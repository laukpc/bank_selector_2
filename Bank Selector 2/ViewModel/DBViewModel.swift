//
//  DBViewModel.swift
//  Bank Selector 2
//
//  Created by Laurynas Kapacinskas on 2021-09-01.
//

import SwiftUI
import RealmSwift

class DBViewModel: ObservableObject {
  
    @Published var name: String = ""
    @Published var imageUri: String = ""
    @Published var imageData: Data? = nil
    @Published var bic: String = ""
    @Published var selected: Bool = false
   
    @Published var banks: [BankDB] = []
    
    init() {
        //print(Realm.Configuration.defaultConfiguration.fileURL!)
    }
    
    func fetchData(){
        guard let realm = try? Realm() else {return}
        
        let results = realm.objects(BankDB.self)
        self.banks = results.compactMap({ (bank) -> BankDB? in
            return bank
        })
    }
    
    func addData(){
        guard let realm = try? Realm() else {return}
        
        self.banks.forEach { bank in
            let bankToAdd = BankDB()
            bankToAdd.name = bank.name
            bankToAdd.bic = bank.bic
            bankToAdd.imageData = bank.imageData
            bankToAdd.imageUri = bank.imageUri
            bankToAdd.selected = bank.selected
            try? realm.write({
                realm.add(bank, update: .all)
            })
        }
    }
}
