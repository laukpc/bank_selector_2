//
//  BankListViewModel.swift
//  Bank Selector 2
//
//  Created by Laurynas Kapacinskas on 2021-08-31.
//

import SwiftUI

class BankListViewModel: ObservableObject {
    @Published var banks = [BankDB]()
    @ObservedObject var dbViewModel = DBViewModel()
    
    init() {
        dbViewModel.fetchData()
        self.banks = dbViewModel.banks
    }
}
