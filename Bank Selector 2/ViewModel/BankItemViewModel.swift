//
//  BankItemViewModel.swift
//  Bank Selector 2
//
//  Created by Laurynas Kapacinskas on 2021-09-02.
//

import SwiftUI
import RealmSwift

class BankItemViewModel: ObservableObject {
    
    @Published var selectedBankName: String = ""

    init() {}
    
    func updateSelected() {
        guard let realm = try? Realm() else {return}
        let filter = "\(selectedBankName)"
        for bank in realm.objects(BankDB.self).filter("name == %@ OR selected == true", filter){
            do {
                try realm.write {
                    bank.selected = !bank.selected
                }
            } catch {
                print("error updating  done status, \(error)")
            }
        }
    }
}
