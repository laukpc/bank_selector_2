//
//  ContentView.swift
//  Bank Selector 2
//
//  Created by Laurynas Kapacinskas on 2021-08-29.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            MainDisplayView(bankDisplayName: "Select a bank")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}



