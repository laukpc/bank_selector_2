//
//  SwiftUIView.swift
//  Bank Selector 2
//
//  Created by Laurynas Kapacinskas on 2021-09-01.
//

import SwiftUI

struct MainDisplayView: View {
    
    @StateObject var viewModel = MainDisplayViewModel()
    
    let bankDisplayName: String?
    
    var body: some View {
        ZStack {
            K.Colors.primary
                .ignoresSafeArea()
            VStack {
                Text(viewModel.selectedBankName ?? "Hello, press button below")
                    .font(.largeTitle)
                    .foregroundColor(.white)
                    .padding(100)
                NavigationLink(
                    destination: BankListView(),
                    label: {
                        Text("See all banks")
                            .foregroundColor(.black)
                            .fontWeight(.bold)
                        
                    })
                    .padding()
                    .background(K.Colors.secondary)
                    .cornerRadius(15)
            }
        }
        .navigationBarHidden(true)
        .onAppear(){
            viewModel.filterForSelected()
        }
    }
}
