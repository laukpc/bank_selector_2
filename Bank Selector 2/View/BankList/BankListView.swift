//
//  BankListView.swift
//  Bank Selector 2
//
//  Created by Laurynas Kapacinskas on 2021-08-30.
//

import SwiftUI

struct BankListView: View {
    
    @StateObject var viewModel = BankListViewModel()
    
    init() {
        UINavigationBar.appearance()
            .largeTitleTextAttributes = [.foregroundColor: UIColor.white]
    }
    
    let layout = [
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    var body: some View {
        ZStack {
            K.Colors.primary
                .ignoresSafeArea()
            ScrollView{
                LazyVGrid(columns: layout, alignment: .center, spacing: 10, content: {
                    ForEach(viewModel.banks, id: \.self) { item in
                        BankItemView(bankName: item.name, imageUrl: item.imageUri)
                    }
                })
            }
        }
        .navigationBarTitle("Banks")
    }
}

struct BankListView_Previews: PreviewProvider {
    static var previews: some View {
        BankListView()
    }
}
