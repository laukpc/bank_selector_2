//
//  BankItemView.swift
//  Bank Selector 2
//
//  Created by Laurynas Kapacinskas on 2021-08-30.
//

import SwiftUI

struct BankItemView: View {
    
    @StateObject var viewModel = BankItemViewModel()
    
    let bankName: String
    let imageUrl: String
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        ZStack {
            Rectangle()
                .foregroundColor(Color.white)
                .cornerRadius(20)
            VStack {
                RemoteImage(url: imageUrl)
                    .aspectRatio(contentMode: .fit)
                Text(bankName)
                    .foregroundColor(K.Colors.secondary)
                    .padding()
            }
        }
        .frame(width: 185, height: 120)
        .onTapGesture {
            //update realm object's selected property
            viewModel.selectedBankName = bankName
            viewModel.updateSelected()
            presentationMode.wrappedValue.dismiss()
        }
    }
}
