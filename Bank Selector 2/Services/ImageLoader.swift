//
//  ImageLoader.swift
//  Bank Selector 2
//
//  Created by Laurynas Kapacinskas on 2021-08-31.
//
import SwiftUI
import Alamofire

final class ImageLoader {
    
    func loadImage(imageUrl: String) -> Data?{
        guard let url = URL(string: imageUrl) else { return nil }
        var data: Data? = nil
        
        AF.request(url).response { response in
            switch response.result {
            case .success:
                if let unwrappedData = response.data {
                    data = unwrappedData
                }
            case .failure:
                print("failed to fetch image")
            }
        }
        return data
    }

    init() {}
}
