//
//  APICaller.swift
//  Bank Selector 2
//
//  Created by Laurynas Kapacinskas on 2021-08-29.
//

import Foundation
import Alamofire

class NetworkManager: ObservableObject {
    
    struct Constants {
        static let topHeadlinesURL = URL(string: "https://www.bio-matic.com/applications/kevin/banks.php?country=LT")
    }
    
    static let shared = NetworkManager()
    
    private init() {}
    
    public func getBankList(completion: @escaping (Result<[BankDB], AFError>) -> Void) {
        guard let url = Constants.topHeadlinesURL else {
            return
        }
        
        AF.request(url, method: .get).responseData(completionHandler: { (response) in
            switch response.result {
            case .success(let res):
                if response.value != nil {
                    let jsonString = String(data: res, encoding: .utf8)!
                    let responseData = Response(JSONString: jsonString)
                    completion(.success(responseData?.data ?? []))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
}
