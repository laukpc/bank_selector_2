//
//  Bank.swift
//  Bank Selector 2
//
//  Created by Laurynas Kapacinskas on 2021-08-31.
//

import Foundation
import ObjectMapper

struct Response: Mappable {
    
    var data: [BankDB]?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
            data <- map["data"]
    }
}

