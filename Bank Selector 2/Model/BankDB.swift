//
//  BankDB.swift
//  Bank Selector 2
//
//  Created by Laurynas Kapacinskas on 2021-08-31.
//
import Foundation
import RealmSwift
import ObjectMapper

class BankDB: Object, Mappable {
    
    let loader = ImageLoader()
    @Persisted var name = ""
    @Persisted var bic = ""
    @Persisted var imageUri = ""
    @Persisted var imageData: Data? = nil
    @Persisted var selected = false
    
    override static func primaryKey() -> String? {
        "name"
    }
    
    required convenience init?(map: ObjectMapper.Map) {
        self.init()
        
    }
    
    func mapping(map: ObjectMapper.Map) {
        name <- map["name"]
        imageUri <- map["imageUri"]
        bic <- map["bic"]
    }
    
}
